/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author cristiano
 */
@Entity
public class UserClient2 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(length = 100)
    private String name;
    
    @Column(length = 40)
    private String token;
    
    @Column(length = 40)
    private String clientToken;
    
    @Column()
    private Boolean synched;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
    }

    public Boolean getSynched() {
        return synched;
    }

    public void setSynched(Boolean synched) {
        this.synched = synched;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 59 * hash + (this.token != null ? this.token.hashCode() : 0);
        hash = 59 * hash + (this.clientToken != null ? this.clientToken.hashCode() : 0);
        hash = 59 * hash + (this.synched != null ? this.synched.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserClient2 other = (UserClient2) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.token == null) ? (other.token != null) : !this.token.equals(other.token)) {
            return false;
        }
        if ((this.clientToken == null) ? (other.clientToken != null) : !this.clientToken.equals(other.clientToken)) {
            return false;
        }
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.synched != other.synched && (this.synched == null || !this.synched.equals(other.synched))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserClient2{" + "id=" + id + ", name=" + name + ", token=" + token + ", clientToken=" + clientToken + ", synched=" + synched + '}';
    }
    
}
