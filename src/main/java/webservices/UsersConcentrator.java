/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.UserConcentrator;
import persistence.UserConcentratorJpaController;

/**
 *
 * @author cristiano
 */
@WebServlet(name = "UsersConcentrator", urlPatterns = {"/usersConcentrator"})
public class UsersConcentrator extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            EntityManagerFactory objFactory = Persistence.createEntityManagerFactory("usersync_usersync_war_1.0PU");

            UserConcentrator               newUser = new UserConcentrator();
            UserConcentratorJpaController  userJpa = new UserConcentratorJpaController(objFactory);

            List<UserConcentrator> usersList = userJpa.findUserConcentratorEntities();
            
            List<Map<String, String>> usersReponse = new ArrayList<Map<String, String>>();
            
            for(UserConcentrator user : usersList) {
                Map<String, String> usersMap = new HashMap<String, String>();
                
                usersMap.put("id", user.getId().toString());
                usersMap.put("name", user.getName());
                usersMap.put("token", user.getToken());
                
                usersReponse.add(usersMap);
            }
            
            String json = new ObjectMapper().writeValueAsString(usersReponse);

            response.setContentType("application/json");

            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write(json);
                    
        } catch (Exception e) {

            // 500 error
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Ops! Ocorreu um erro ao tentar recuperar os usuários/n"
                    + "error: " + e);
        }
    }
}
