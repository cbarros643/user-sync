/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import helpers.TokenGenerator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.UserClient1;
import models.UserConcentrator;
import persistence.UserClient1JpaController;
import persistence.UserConcentratorJpaController;

/**
 *
 * @author cristiano
 */
@WebServlet(name = "UserClient1", urlPatterns = {"/userClient1"})
public class UserClient1WS extends HttpServlet {
    
    private final String CLIENTE_TOKEN = "p04eqgqlenrbgk7mu924djre64";

    /*@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }*/
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            EntityManagerFactory objFactory = Persistence.createEntityManagerFactory("usersync_usersync_war_1.0PU");

            UserClient1              newUser = new UserClient1();
            UserClient1JpaController userJpa = new UserClient1JpaController(objFactory);

            newUser.setName(request.getParameterMap().get("name")[0]);
            newUser.setToken(TokenGenerator.newToken());
            newUser.setClientToken(CLIENTE_TOKEN);
            newUser.setSynched(false);

            userJpa.create(newUser);

            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write("Usuário cadastrado com sucesso");
                    
        } catch (Exception e) {

            // 500 error
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Ops! Ocorreu um erro ao tentar cadastrar o usuário/n"
                    + "error: " + e);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            EntityManagerFactory objFactory = Persistence.createEntityManagerFactory("usersync_usersync_war_1.0PU");

            UserClient1              newUser = new UserClient1();
            UserClient1JpaController userJpa = new UserClient1JpaController(objFactory);

            List<UserClient1> usersList = userJpa.findUserClient1Entities();
            
            List<Map<String, String>> usersReponse = new ArrayList<Map<String, String>>();
            
            for(UserClient1 user : usersList) {
                Map<String, String> usersMap = new HashMap<String, String>();
                
                usersMap.put("id", user.getId().toString());
                usersMap.put("name", user.getName());
                usersMap.put("token", user.getToken());
                usersMap.put("synched", user.getSynched().toString());
                
                usersReponse.add(usersMap);
            }
            
            String json = new ObjectMapper().writeValueAsString(usersReponse);

            response.setContentType("application/json");

            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write(json);
                    
        } catch (Exception e) {

            // 500 error
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Ops! Ocorreu um erro ao tentar recuperar os usuários/n"
                    + "error: " + e);
        }
    }
    
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
        try {
            EntityManagerFactory objFactory = Persistence.createEntityManagerFactory("usersync_usersync_war_1.0PU");
            
            // Client Entity manager
            UserClient1              userClient    = new UserClient1();
            UserClient1JpaController userClientJpa = new UserClient1JpaController(objFactory);
            
            List<UserClient1> usersList = userClientJpa.findUserClient1Entities();
            
            // Concentrator Entity manager
            UserConcentrator              userConcentrator    = new UserConcentrator();
            UserConcentratorJpaController userConcentratorJpa = new UserConcentratorJpaController(objFactory);
            
            List<UserConcentrator> curUsers = userConcentratorJpa.findUserConcentratorEntities();
            
            // Update the client
            for(UserConcentrator curUser : curUsers) {

                if (curUser.getClientToken().equals(CLIENTE_TOKEN)) {
                    
                    Boolean condition = true;
                    
                    if (usersList.size() > 0) {
                        
                        for(UserClient1 user : usersList) {
                        
                            if (user.getSynched() == true && user.getToken().equals(curUser.getToken())) {
                                condition = false;
                            }

                        }
                    }
                        
                    if (condition) {
                        userClient.setName(curUser.getName());
                        userClient.setToken(curUser.getToken());
                        userClient.setClientToken(curUser.getClientToken());
                        userClient.setSynched(Boolean.TRUE);

                        userClientJpa.create(userClient);
                    }
                    
                }
            }
                
            // Update the concentrator
            for(UserClient1 user : usersList) {
                if (user.getSynched() == false) {
                    userConcentrator.setName(user.getName());
                    userConcentrator.setToken(user.getToken());
                    userConcentrator.setClientToken(user.getClientToken());
                    
                    user.setSynched(Boolean.TRUE);

                    userConcentratorJpa.create(userConcentrator);
                    
                    userClientJpa.edit(user);
                }
                
            }

            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write("Usuários sincronizados com sucesso");
            
        } catch (Exception e) {

            // 500 error
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Ops! Ocorreu um erro ao tentar sincronizar os usuários/n"
                    + "error: " + e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        try {
           
            EntityManagerFactory objFactory = Persistence.createEntityManagerFactory("usersync_usersync_war_1.0PU");
            EntityManager manager = objFactory.createEntityManager();
            UserClient1JpaController userJpa = new UserClient1JpaController(objFactory);

            List<UserClient1> usersList = userJpa.findUserClient1Entities();
            
            for(UserClient1 user : usersList) {
            
                userJpa.destroy(user.getId());
            }

            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write("Usuários excluídos com sucesso");
            
        } catch (Exception e) {

            // 500 error
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Ops! Ocorreu um erro ao tentar excluir os usuários/n"
                    + "error: " + e);
        }
    }
}
