/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import helpers.TokenGenerator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.UserClient2;
import models.UserConcentrator;
import persistence.UserClient2JpaController;
import persistence.UserConcentratorJpaController;

/**
 *
 * @author cristiano
 */
@WebServlet(name = "UserClient2", urlPatterns = {"/userClient2"})
public class UserClient2WS extends HttpServlet {
    
    private final String CLIENTE_TOKEN = "h3uhvm922jiqn8s0ukqgt46lc0";
    
    /*@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }*/
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            EntityManagerFactory objFactory = Persistence.createEntityManagerFactory("usersync_usersync_war_1.0PU");

            UserClient2              newUser = new UserClient2();
            UserClient2JpaController userJpa = new UserClient2JpaController(objFactory);

            newUser.setName(request.getParameterMap().get("name")[0]);
            newUser.setToken(TokenGenerator.newToken());
            newUser.setClientToken(CLIENTE_TOKEN);
            newUser.setSynched(false);

            userJpa.create(newUser);

            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write("Usuário cadastrado com sucesso");
                    
        } catch (Exception e) {

            // 500 error
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Ops! Ocorreu um erro ao tentar cadastrar o usuário/n"
                    + "error: " + e);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            EntityManagerFactory objFactory = Persistence.createEntityManagerFactory("usersync_usersync_war_1.0PU");

            UserClient2              newUser = new UserClient2();
            UserClient2JpaController userJpa = new UserClient2JpaController(objFactory);

            List<UserClient2> usersList = userJpa.findUserClient2Entities();
            
            List<Map<String, String>> usersReponse = new ArrayList<Map<String, String>>();
            
            for(UserClient2 user : usersList) {
                Map<String, String> usersMap = new HashMap<String, String>();
                
                usersMap.put("id", user.getId().toString());
                usersMap.put("name", user.getName());
                usersMap.put("token", user.getToken());
                usersMap.put("synched", user.getSynched().toString());
                
                usersReponse.add(usersMap);
            }
            
            String json = new ObjectMapper().writeValueAsString(usersReponse);

            response.setContentType("application/json");

            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write(json);
                    
        } catch (Exception e) {

            // 500 error
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Ops! Ocorreu um erro ao tentar recuperar os usuários/n"
                    + "error: " + e);
        }
    }
    
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
        try {
            EntityManagerFactory objFactory = Persistence.createEntityManagerFactory("usersync_usersync_war_1.0PU");
            
            // Client Entity manager
            UserClient2              userClient    = new UserClient2();
            UserClient2JpaController userClientJpa = new UserClient2JpaController(objFactory);
            
            List<UserClient2> usersList = userClientJpa.findUserClient2Entities();
            
            // Concentrator Entity manager
            UserConcentrator              userConcentrator    = new UserConcentrator();
            UserConcentratorJpaController userConcentratorJpa = new UserConcentratorJpaController(objFactory);
            
            List<UserConcentrator> curUsers = userConcentratorJpa.findUserConcentratorEntities();
            
            // Update the client
            for(UserConcentrator curUser : curUsers) {

                if (curUser.getClientToken().equals(CLIENTE_TOKEN)) {
                    
                    Boolean condition = true;
                    
                    if (usersList.size() > 0) {
                        
                        for(UserClient2 user : usersList) {
                        
                            if (user.getSynched() == true && user.getToken().equals(curUser.getToken())) {
                                condition = false;
                            }

                        }
                    }
                        
                    if (condition) {
                        userClient.setName(curUser.getName());
                        userClient.setToken(curUser.getToken());
                        userClient.setClientToken(curUser.getClientToken());
                        userClient.setSynched(Boolean.TRUE);

                        userClientJpa.create(userClient);
                    }
                    
                }
            }
                
            // Update the concentrator
            for(UserClient2 user : usersList) {
                if (user.getSynched() == false) {
                    userConcentrator.setName(user.getName());
                    userConcentrator.setToken(user.getToken());
                    userConcentrator.setClientToken(user.getClientToken());
                    
                    user.setSynched(Boolean.TRUE);

                    userConcentratorJpa.create(userConcentrator);
                    
                    userClientJpa.edit(user);
                }
                
            }

            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write("Usuários sincronizados com sucesso");
            
        } catch (Exception e) {

            // 500 error
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Ops! Ocorreu um erro ao tentar sincronizar os usuários/n"
                    + "error: " + e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        try {
           
            EntityManagerFactory objFactory = Persistence.createEntityManagerFactory("usersync_usersync_war_1.0PU");
            EntityManager manager = objFactory.createEntityManager();
            UserClient2JpaController userJpa = new UserClient2JpaController(objFactory);

            List<UserClient2> usersList = userJpa.findUserClient2Entities();
            
            for(UserClient2 user : usersList) {
            
                userJpa.destroy(user.getId());
            }

            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_CREATED);
            response.getWriter().write("Usuários excluídos com sucesso");
            
        } catch (Exception e) {

            // 500 error
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Ops! Ocorreu um erro ao tentar excluir os usuários/n"
                    + "error: " + e);
        }
    }
}
