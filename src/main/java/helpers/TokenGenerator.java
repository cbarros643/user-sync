/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 *
 * @author cristiano
 */
public class TokenGenerator {

    public static String newToken() {
      return new BigInteger(130, new SecureRandom()).toString(32);
    }
}
