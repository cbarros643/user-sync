/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.UserClient1;
import persistence.exceptions.NonexistentEntityException;

/**
 *
 * @author cristiano
 */
public class UserClient1JpaController implements Serializable {

    public UserClient1JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserClient1 userClient1) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(userClient1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserClient1 userClient1) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            userClient1 = em.merge(userClient1);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = userClient1.getId();
                if (findUserClient1(id) == null) {
                    throw new NonexistentEntityException("The userClient1 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserClient1 userClient1;
            try {
                userClient1 = em.getReference(UserClient1.class, id);
                userClient1.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userClient1 with id " + id + " no longer exists.", enfe);
            }
            em.remove(userClient1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserClient1> findUserClient1Entities() {
        return findUserClient1Entities(true, -1, -1);
    }

    public List<UserClient1> findUserClient1Entities(int maxResults, int firstResult) {
        return findUserClient1Entities(false, maxResults, firstResult);
    }

    private List<UserClient1> findUserClient1Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserClient1.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserClient1 findUserClient1(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserClient1.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserClient1Count() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserClient1> rt = cq.from(UserClient1.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
