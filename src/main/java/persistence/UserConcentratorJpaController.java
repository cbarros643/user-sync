/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.UserConcentrator;
import persistence.exceptions.NonexistentEntityException;

/**
 *
 * @author cristiano
 */
public class UserConcentratorJpaController implements Serializable {

    public UserConcentratorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserConcentrator userConcentrator) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(userConcentrator);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserConcentrator userConcentrator) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            userConcentrator = em.merge(userConcentrator);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = userConcentrator.getId();
                if (findUserConcentrator(id) == null) {
                    throw new NonexistentEntityException("The userConcentrator with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserConcentrator userConcentrator;
            try {
                userConcentrator = em.getReference(UserConcentrator.class, id);
                userConcentrator.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userConcentrator with id " + id + " no longer exists.", enfe);
            }
            em.remove(userConcentrator);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserConcentrator> findUserConcentratorEntities() {
        return findUserConcentratorEntities(true, -1, -1);
    }

    public List<UserConcentrator> findUserConcentratorEntities(int maxResults, int firstResult) {
        return findUserConcentratorEntities(false, maxResults, firstResult);
    }

    private List<UserConcentrator> findUserConcentratorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserConcentrator.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserConcentrator findUserConcentrator(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserConcentrator.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserConcentratorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserConcentrator> rt = cq.from(UserConcentrator.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
