/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.UserClient2;
import persistence.exceptions.NonexistentEntityException;

/**
 *
 * @author cristiano
 */
public class UserClient2JpaController implements Serializable {

    public UserClient2JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserClient2 userClient2) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(userClient2);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserClient2 userClient2) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            userClient2 = em.merge(userClient2);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = userClient2.getId();
                if (findUserClient2(id) == null) {
                    throw new NonexistentEntityException("The userClient2 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserClient2 userClient2;
            try {
                userClient2 = em.getReference(UserClient2.class, id);
                userClient2.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userClient2 with id " + id + " no longer exists.", enfe);
            }
            em.remove(userClient2);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserClient2> findUserClient2Entities() {
        return findUserClient2Entities(true, -1, -1);
    }

    public List<UserClient2> findUserClient2Entities(int maxResults, int firstResult) {
        return findUserClient2Entities(false, maxResults, firstResult);
    }

    private List<UserClient2> findUserClient2Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserClient2.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserClient2 findUserClient2(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserClient2.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserClient2Count() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserClient2> rt = cq.from(UserClient2.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
