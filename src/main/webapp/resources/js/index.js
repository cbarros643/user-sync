/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Client user create
var clientNewUser = function(event) {
    event.preventDefault();
    
    var sectionBlock = $(this).parents("section");
    
    var user = {
        name:  $(this).parents("form").find("input").val()
    };
    
    var endpoint = $(this).attr("data-endpoint");
    
    $.ajax({
        url:      "/usersync" + endpoint,
        type:     "POST",
        data:     user
    })
    .done(function( data ) {
        alert(data);
        getUsers(endpoint, sectionBlock);
    })
    .fail(function( data ) {
        alert(data);
    });
};

$("#client-1").find("form button").click(clientNewUser);
$("#client-2").find("form button").click(clientNewUser);

// Users list refresh
function refreshUserList(listBlock, usersObj) {
    var source   = $("#user-template").html();
    var template = Handlebars.compile(source);
    
    listBlock.find("table tbody")
            .empty()
            .html(template(usersObj));
}

function getUsers(endpoint, listBlock) {
    
    $.ajax({
        url:      "/usersync" + endpoint,
        type:     "GET"
    })
    .done(function( data ) {
        refreshUserList(listBlock, data);
    })
    .fail(function( data ) {
    });
    
}

getUsers("/userClient1", $("#client-1"));
getUsers("/userClient2", $("#client-2"));
getUsers("/usersConcentrator", $("#concentrator"));

// Client users sincronization
var clientSyncUsers = function(event) {
    event.preventDefault();
    
    var sectionBlock = $(this).parents("section");
    var endpoint = $(this).attr("data-endpoint");
    
    $.ajax({
        url:      "/usersync" + endpoint,
        type:     "PUT"
    })
    .done(function( data ) {
        alert(data);
        getUsers(endpoint, sectionBlock);
        getUsers("/usersConcentrator", $("#concentrator"));
    })
    .fail(function( data ) {
        alert(data);
    });
};

$("#client-1").find("#sync1").click(clientSyncUsers);
$("#client-2").find("#sync2").click(clientSyncUsers);

// Client users delete
var deleteUsers = function(event) {
    event.preventDefault();
    
    var sectionBlock = $(this).parents("section");
    var endpoint = $(this).attr("data-endpoint");
    
    $.ajax({
        url:      "/usersync" + endpoint,
        type:     "DELETE"
    })
    .done(function( data ) {
        alert(data);
        getUsers(endpoint, sectionBlock);
    })
    .fail(function( data ) {
        alert(data);
    });
};

$("#client-1").find("#delete1").click(deleteUsers);
$("#client-2").find("#delete2").click(deleteUsers);